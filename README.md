# only-app-fx

#### 项目介绍
这是一款javafx的主题，以及
无标题窗口OnlyStage
靠边自动隐藏OnlyWindowAutoHide

#### 截图

* 整体效果
<img src="./screenshot/0.png">
<br>

* 和原版对比截图
<img src="./screenshot/1.png">
<br>
<img src="./screenshot/2.png">
<br>
<img src="./screenshot/3.png">
<br>
<img src="./screenshot/4.png">
<br>
<img src="./screenshot/5.png">
<br>
<img src="./screenshot/6.png">
<br>
<img src="./screenshot/6-1.png">
<br>
<img src="./screenshot/6-2.png">

<br>
<img src="./screenshot/8.png">
<br>

<center>
<figure>
<img src="./screenshot/9-1.png">
<img src="./screenshot/9-2.png">
</figure>
</center>

<br>

<center>
<figure>
<img src="./screenshot/10-1.png">
<img src="./screenshot/10-2.png">
</figure>
</center>

<br>

<center>
<figure>
<img src="./screenshot/11-1.png">
<img src="./screenshot/11-2.png">
</figure>
</center>

<br>

<center>
<figure>
<img src="./screenshot/12-1.png">
<img src="./screenshot/12-2.png">
</figure>
</center>

<br>
<img src="./screenshot/13.png">
<br>
<img src="./screenshot/14.png">
<br>
<img src="./screenshot/16.png">
<br>
<img src="./screenshot/17.png">
<br>
<img src="./screenshot/18.png">
<br>

<center>
<figure>
<img src="./screenshot/18-1.png">
<img src="./screenshot/18-2.png">
</figure>
</center>

<br>
<img src="./screenshot/19.png">

#### 使用说明

```
<dependency>
	<groupId>com.onlyxiahui.app</groupId>
	<artifactId>only-app-fx</artifactId>
	<version>1.0.0</version>
</dependency>
```

```Java
package demo;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * 
 * 只使用css方式，导入css的组件的子组件有效<br>
 * Date 2020-06-19 16:33:34<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class CssSample extends Application {

	@Override
	public void start(Stage stage) {
		Scene scene = new Scene(new Group());
		scene.getStylesheets().add(CssSample.class.getResource("/com/only/common/css/Only.css").toString());

		stage.setTitle("Hello world");
		stage.setWidth(450);
		stage.setHeight(550);

		Button button = new Button("Hello world");

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(button);

		((Group) scene.getRoot()).getChildren().addAll(vbox);

		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
```

```Java
package demo;

import com.sun.javafx.css.StyleManager;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * 
 * 全局使用，所有组件都起效果<br>
 * Date 2020-06-19 16:36:35<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class GlobalSample extends Application {

	@Override
	public void start(Stage stage) {
		Scene scene = new Scene(new Group());

		stage.setTitle("Hello world");
		stage.setWidth(450);
		stage.setHeight(550);

		Button button = new Button("Hello world");

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(button);

		((Group) scene.getRoot()).getChildren().addAll(vbox);

		stage.setScene(scene);
		stage.show();

		// 此方式需要stage.show();执行后再执行才起效果
		StyleManager.getInstance().addUserAgentStylesheet(GlobalSample.class.getResource("/com/only/common/css/Only.css").toString());
	}

	public static void main(String[] args) {
		launch(args);
	}
}
```


```Java
package demo;

import com.onlyxiahui.app.fx.OnlyStage;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * 
 * OnlyStage使用方式，所有OnlyStage内的组件有效<br>
 * Date 2020-06-19 16:31:54<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class OnlyStageSample extends Application {

	OnlyStage onlyStage = new OnlyStage();

	@Override
	public void start(Stage stage) {
		onlyStage.setTitle("Hello world");
		onlyStage.setWidth(450);
		onlyStage.setHeight(550);

		Button button = new Button("Hello world");

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(button);
		onlyStage.setCenter(vbox);
		onlyStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
```
