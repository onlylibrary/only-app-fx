package com.onlyxiahui.app.fx;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * 
 * Description <br>
 * Date 2020-06-19 11:33:00<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class OnlyDecoratedButton extends Button {

	ImageView imageView = new ImageView();

	private Image normalImage = null;
	private Image hoverImage = null;
	private Image pressedImage = null;

	public OnlyDecoratedButton() {
		initIconStyleButton();
		initEvent();
	}

	public OnlyDecoratedButton(Image normalImage, Image hoverImage, Image pressedImage) {
		this.normalImage = normalImage;
		this.hoverImage = hoverImage;
		this.pressedImage = pressedImage;
		initIconStyleButton();
		initEvent();
		updateImage();
	}

	private void initIconStyleButton() {
		this.setGraphic(imageView);

		imageView.getStyleClass().add("image-show");
	}

	private void initEvent() {
		this.hoverProperty().addListener(h -> {
			updateImage();
		});

		this.pressedProperty().addListener(p -> {
			updateImage();
		});
	}

	public void setNormalImage(Image value) {
		normalImage = value;
		updateImage();
	}

	public void setHoverImage(Image value) {
		hoverImage = value;
	}

	public void setPressedImage(Image value) {
		pressedImage = value;
	}

	private void updateImage() {
		boolean pressed = this.hoverProperty().getValue();
		boolean hover = this.hoverProperty().getValue();
		if (null != pressedImage && pressed) {
			imageView.setImage(pressedImage);
		} else if (null != hoverImage && hover) {
			imageView.setImage(hoverImage);
		} else {
			imageView.setImage(normalImage);
		}
	}

	public void setImage(Image normalImage, Image hoverImage, Image pressedImage) {
		this.normalImage = normalImage;
		this.hoverImage = hoverImage;
		this.pressedImage = pressedImage;
		updateImage();
	}
}
