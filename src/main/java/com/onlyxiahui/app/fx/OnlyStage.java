/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlyxiahui.app.fx;

import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * 
 * Description <br>
 * Date 2020-06-19 11:38:43<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class OnlyStage extends Stage {

	public OnlyStage() {
		this(StageStyle.TRANSPARENT);
	}

	public OnlyStage(StageStyle style) {
		super(style);
		initStage();
		initEvent();
	}

	// private final GaussianBlur gaussianBlur = new GaussianBlur();

	private final DropShadow dropShadow = new DropShadow();
	private final BorderPane rootPane = new BorderPane();

	private final StackPane backgroundColorPane = new StackPane();
	protected final StackPane backgroundImagePane = new StackPane();
	private final StackPane stackPane = new StackPane();
	private final BorderPane centerPane = new BorderPane();
	private final Rectangle clip = new Rectangle();
	private final OnlyScene scene = new OnlyScene(getRootPane());
	private final OnlyDecoratedPane decoratedPane = new OnlyDecoratedPane(this, scene, 1);
	private Insets normalInsets = new Insets(8, 8, 8, 8);
	private Insets maxInsets = new Insets(0, 0, 0, 0);

	private void initStage() {
		this.setScene(scene);

		scene.setFill(Color.TRANSPARENT);

		dropShadow.setBlurType(BlurType.GAUSSIAN);
		// dropShadow.setRadius(20);
		dropShadow.setColor(new Color(0.50, 0.50, 0.50, 0.6));

		rootPane.setBackground(Background.EMPTY);
		rootPane.setEffect(dropShadow);
		// rootPane.getChildren().add(stackPane);
		rootPane.setCenter(stackPane);
		rootPane.setPadding(normalInsets);
		rootPane.setStyle("-fx-background-color: null;");

		stackPane.setClip(clip);
		stackPane.widthProperty().addListener((Observable observable) -> {
			clip.setWidth(stackPane.getWidth());
		});
		stackPane.heightProperty().addListener((Observable observable) -> {
			clip.setHeight(stackPane.getHeight());
		});

		backgroundColorPane.setStyle("-fx-background-color:rgba(255, 255, 255, 1)");

		stackPane.getChildren().add(backgroundColorPane);
		stackPane.getChildren().add(backgroundImagePane);
		stackPane.getChildren().add(centerPane);
		stackPane.getChildren().add(getOnlyDecoratedPane());

		// setRadius(3);
		// setDropShadowRadius(6);
		// setSpread(0.12);
	}

	private void initEvent() {
		this.maximizedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue) {
					rootPane.setPadding(maxInsets);
				} else {
					rootPane.setPadding(normalInsets);
				}
			}
		});
	}

	public void setCenter(Node value) {
		centerPane.setCenter(value);
		// centerPane.getChildren().clear();
		// centerPane.getChildren().add(value);
	}

	public void setBackgroundColor(Color color) {
		centerPane.setStyle("-fx-background-color:rgba(" + color.getRed() * 255 + "," + color.getGreen() * 255 + "," + color.getBlue() * 255 + ", " + color.getOpacity() + ")");
	}

	public void setRadius(double value) {
		clip.setArcHeight(value);
		clip.setArcWidth(value);
	}

	public void setDropShadowRadius(double value) {
		dropShadow.setRadius(value);
	}

	public void setSpread(double value) {
		dropShadow.setSpread(value);
	}

	public OnlyDecoratedPane getOnlyDecoratedPane() {
		return decoratedPane;
	}

	public BorderPane getRootPane() {
		return rootPane;
	}

	public void setTitlePaneStyle(int style) {
		decoratedPane.setTitlePaneStyle(style);
	}
}
