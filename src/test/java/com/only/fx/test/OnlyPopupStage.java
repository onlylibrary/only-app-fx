/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.only.fx.test;

import com.onlyxiahui.app.fx.OnlyPopup;
import com.onlyxiahui.app.fx.OnlyStage;

import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author XiaHui
 */
public class OnlyPopupStage extends OnlyStage {

	BorderPane rootPane = new BorderPane();
	OnlyPopup po = new OnlyPopup();

	public OnlyPopupStage() {
		init();
	}

	private void init() {
		this.setCenter(rootPane);
		this.setTitle(" OnlyPopupOver");
		this.setWidth(380);
		this.setHeight(600);
		this.setRadius(10);

		//po.setDetached(false);
		//po.setDetachable(false);

		Button button = new Button("确定");
		button.setPrefSize(80, 25);

		HBox hb = new HBox();
		// hb.setStyle("-fx-background-color:rgba(0, 0, 0, 1)");
		hb.setPrefSize(100, 100);

		hb.getChildren().add(button);
		// hb.setMaxHeight(50);
		
		po.setArrowLocation(OnlyPopup.ArrowLocation.TOP_CENTER);
		po.setContentNode(hb);
		po.setArrowSize(0);

		rootPane.setOnMouseClicked(m -> {
			po.show(OnlyPopupStage.this, m.getScreenX(), m.getScreenY());
		});
	}
}
