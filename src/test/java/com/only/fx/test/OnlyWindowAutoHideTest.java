/*
 * To change mainFrame license header, choose License Headers in Project Properties.
 * To change mainFrame template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.only.fx.test;

import com.onlyxiahui.app.fx.OnlyStage;
import com.onlyxiahui.app.fx.OnlyWindowAutoHide;

import javafx.application.Application;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author Only
 */
public class OnlyWindowAutoHideTest extends Application {
	OnlyStage stage = new OnlyStage();
	final DropShadow dropShadow = new DropShadow();

	@Override
	public void start(Stage s) {
		BorderPane rootPane = new BorderPane();

		stage.show();
		stage.setTitle("AutoHide");
		stage.setWidth(900);
		stage.setHeight(560);
		stage.setMinWidth(1000);
		stage.setMinHeight(530);

		HBox hBox = new HBox();

		OnlyWindowAutoHide oh = new OnlyWindowAutoHide(stage);

		Button t = new Button("桌面顶部隐藏");
		Button r = new Button("桌面右边隐藏");
		Button b = new Button("桌面底部隐藏");
		Button l = new Button("桌面左部隐藏");
		Button all = new Button("桌面所有边隐藏");

		Button show = new Button("显示");

		hBox.getChildren().add(t);
		hBox.getChildren().add(r);
		hBox.getChildren().add(b);
		hBox.getChildren().add(l);
		hBox.getChildren().add(all);
		hBox.getChildren().add(show);

		rootPane.setCenter(hBox);

		rootPane.setOnMouseMoved(m -> {
			m.consume();
		});

		rootPane.setOnMouseExited(m -> {
			m.consume();
		});

		hBox.setOnMouseExited(m -> {
			m.consume();
		});
		t.setOnAction(a -> {
			oh.setHidePosition(OnlyWindowAutoHide.Position.TOP);
			// oh.hide();
		});
		r.setOnAction(a -> {
			oh.setHidePosition(OnlyWindowAutoHide.Position.RIGHT);
			// oh.hide();
		});
		b.setOnAction(a -> {
			oh.setHidePosition(OnlyWindowAutoHide.Position.BOTTOM);
			// oh.hide();
		});
		l.setOnAction(a -> {
			oh.setHidePosition(OnlyWindowAutoHide.Position.LEFT);
			// oh.hide();
		});
		all.setOnAction(a -> {
			oh.setHidePosition(OnlyWindowAutoHide.Position.ALL);
			// oh.hide();
		});
		show.setOnAction(a -> {
			oh.show();
		});

		stage.setCenter(rootPane);
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
