package demo;

import com.onlyxiahui.app.fx.OnlyStage;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * 
 * OnlyStage使用方式，所有OnlyStage内的组件有效<br>
 * Date 2020-06-19 16:31:54<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class OnlyStageSample extends Application {

	OnlyStage onlyStage = new OnlyStage();

	@Override
	public void start(Stage stage) {
		onlyStage.setTitle("Hello world");
		onlyStage.setWidth(450);
		onlyStage.setHeight(550);

		Button button = new Button("Hello world");

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(button);
		onlyStage.setCenter(vbox);
		onlyStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}