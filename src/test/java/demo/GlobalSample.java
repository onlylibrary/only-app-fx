package demo;

import com.sun.javafx.css.StyleManager;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * 
 * 全局使用，所有组件都起效果<br>
 * Date 2020-06-19 16:36:35<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class GlobalSample extends Application {

	@Override
	public void start(Stage stage) {
		Scene scene = new Scene(new Group());

		stage.setTitle("Hello world");
		stage.setWidth(450);
		stage.setHeight(550);

		Button button = new Button("Hello world");

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(button);

		((Group) scene.getRoot()).getChildren().addAll(vbox);

		stage.setScene(scene);
		stage.show();

		// 此方式需要stage.show();执行后再执行才起效果
		StyleManager.getInstance().addUserAgentStylesheet(GlobalSample.class.getResource("/com/only/common/css/Only.css").toString());
	}

	public static void main(String[] args) {
		launch(args);
	}
}