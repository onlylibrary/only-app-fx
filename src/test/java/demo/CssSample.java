
package demo;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * 
 * 只使用css方式，导入css的组件的子组件有效<br>
 * Date 2020-06-19 16:33:34<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class CssSample extends Application {

	@Override
	public void start(Stage stage) {
		Scene scene = new Scene(new Group());
		scene.getStylesheets().add(CssSample.class.getResource("/com/only/common/css/Only.css").toString());

		stage.setTitle("Hello world");
		stage.setWidth(450);
		stage.setHeight(550);

		Button button = new Button("Hello world");

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(button);

		((Group) scene.getRoot()).getChildren().addAll(vbox);

		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}